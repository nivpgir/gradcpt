% landauLab addition
% computation of the REAL rate of presentation consists of:
%1. frame per pics (how many steps between trial n pic and n+1 
%2. variable 'rate' which in fact is frame duration per step
%3. hold time = picture duration in clear view (default: as #2)

% how do we convert this fun into actual rate (Hz) of presentation?
%  Hz = 1 / [frame per pic * 'rate' (actually frame duration)]

%what would we want our Hz range to be?
%

% other constraints: we probably should make sure all our subjects get the
% same 'hold time' --> lets aim for either 20 or 30 ms.
% also, fix frameDur to be (1/refreshrate), then FramesPerPic would be set
% according to the Rate (of the picture change), e.g: 
% 2Hz Rate would mean FramesPerPic = refreshRate/2
% 3Hz Rate would mean FramesPerPic = refreshRate/3
% etc...
% 



% Updated for scanning 9/17/10
% updated for resize mse, 9/20/10
% updated for larger resize, 32-channel projector 1/12/12 mse
%new code: lines 21-22  152-155  173-176  191-199  249-252  266-274

clear all; ClockRandSeed;

%%%%%%%%%%%%%%response codes

r1=32; % ' ' for response key 1 (cat2)     male/city

r2=29;  % '.' for response key 2 (cat1)     female/mountain

r1_MRI=21; % red button for response key 1 (cat2)     male/city

r2_MRI=10;  % green button for response key 2 (cat1)     female/mountain



y_size=300; %columns/width % good for 1024 x 768 res
x_size=300; %rows/height


%%%%%%%%%%%%%%%%%%%%%%%%%%dialogue box


%tips for setting the pic rate:
    % Rate*FramesPerPic is the time the picture will take to be fully shown
    % Hold is the time the picture will wait before it will start fading
    % Duration is the time of the whole experiment in seconds
    
prompt = {'Enter subject name','Rate [Hz]','Duration [sec]','Hold','fMRI','Prac','FHR','Prob','Scram','Task'};
def={'XXX','2','30','.03','0','0','0','.9','0','2'};
%!launch the input dialog to get all the starting parameters:
answer = inputdlg(prompt, 'Experimental setup information',1,def); 
%!get the info from the dialog and put it in the correspinding variables:
[subName, Rate, duration, Hold, fMRI, Prac, FHR, Prob, Scram, Task] = deal(answer{:}); 


%%% the data in the variables is a string, and some of it should be turned
%%% into a numbers, thus the next block:
%norm=str2num(norm);
norm=1; %?whats this var?
Rate=str2num(Rate); %!the rate at which pictures are changed
duration=str2num(duration); %!the duration of the expirement in seconds
HoldTime=str2num(Hold); %!the amount of time the picture stays
fMRI=str2num(fMRI); %! 1 if using and MRI 0 if not
FHR=str2num(FHR); %?whats this var?
%Screen_no=str2num(Screen_no);
Screen_no=0; %?whats this var?
Prob=str2num(Prob); % the probability to get a city picture
%Flip=str2num(Flip); %?whats this var?
Flip=0;
Scram=str2num(Scram); %?whats this var?
Task=str2num(Task); %1=go-nogo face gender %2=go-nogo scene type %3=2AFC face gender %4=2AFC scene type
%framesPerPic=str2num(framesPerPic); %!Frames Per Picture
%luminance=str2num(lum); %!self explanatory, if not: https://en.wikipedia.org/wiki/Luminance
luminance=110;


%?the F?
if Prob==.9
    Prob=10;
elseif Prob==.5
    Prob=2;
end;%frequency of city/mountain parameter

facey=FHR; %?whats this var?
housey=1-FHR; %?whats this var?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% get all the picture dirs and names
rootdir=pwd; % pwd returns the current directory path, this will be the path to the scripts dir (pwd is short for Print Worling Directory)
picDir_S={[rootdir '\scenes5\mountain\'] [rootdir '\scenes5\city\']}; %? why is it picDir_*S*
%! turns the path of a directory to a path to a pic and then the 'dir' turns it to a vector of paths to a pic
scenes_cat1=dir([picDir_S{1} '*.jpg']); %! a vector of the mounntain pics paths
scenes_cat2=dir([picDir_S{2} '*.jpg']); %! a vector of the city pics paths

picDir_SR=[rootdir '\scenes5\scrambled\']; 
scrambled_scenes=dir([picDir_SR '*.jpg']);

picDir_SF=[rootdir '\faces\scrambled\'];
scrambled_faces=dir([picDir_SF '*.jpg']);

picDir_F={[rootdir '\faces\female\'] [rootdir '\faces\male\']};
faces_cat1=dir([picDir_F{1} '*.bmp']);
faces_cat2=dir([picDir_F{2} '*.bmp']);


if Scram==1     %scramble scene
    picture_names={scenes_scrambled scenes_scrambled faces_cat1 faces_cat2};   
    picDir_list={picDir_SR picDir_SR picDir_F{1} picDir_F{2}};

elseif Scram==2  %scramble face
    picture_names={scenes_cat1 scenes_cat2 scrambled_faces scrambled_faces};   
    picDir_list={picDir_S{1} picDir_S{2} picDir_SF picDir_SF};

else
    picture_names={scenes_cat1 scenes_cat2 faces_cat1 faces_cat2};
    picDir_list={picDir_S{1} picDir_S{2} picDir_F{1} picDir_F{2}};

end;


image_sum=[0 0 0];

%%%%%%%%%%%%%%%%%%%%%%%%%% buttons and trigger

% if fMRI
%     [KB_id] = FORPInit();
%     [trigger_id] = TriggerInit();
% else
%     %[KB_id] = FORPInit(); %just for testing
%     [KB_id]=KBInit;
% end;


%%%%%%%%%%%%%%%%%%%%%%%%%%% screen and timing preparation

window=Screen(Screen_no,'OpenWindow');


HideCursor;

Screen refresh

%get screen framerate
%hz = FrameRate(window);
%refresh=1/hz;

refreshRate = FrameRate(window);
frameDur = 1/refreshRate;
%Rate=round(Rate/refreshRate)*refreshRate; %is this necessary?
framesPerPic = round(refreshRate/Rate); % = framesPerSec / PicsPerSec
HoldTime = round(HoldTime/frameDur)*frameDur;

%calculate the number of trials according to the duration
numberoftrials=round(duration/(frameDur*(framesPerPic-1)+HoldTime));

% calculate the actual duration time
duration=numberoftrials*(frameDur*(framesPerPic-1)+HoldTime);

WaitTime=Rate; %unnecessary but also harmless



%read the image file
jpeg_face = imread([picDir_F{1} faces_cat1(1,1).name]);
%resize it to wanted size%%%%%%
jpeg_face=imresize(jpeg_face,[x_size,y_size]);


%pick a random face and scene- T1
face_type=randi(Prob);  if face_type>2  face_type=2; end;
face_r=randi(size(picture_names{face_type+2},1));
scene_type=randi(Prob); if scene_type>2  scene_type=2; end;
scene_r=randi(size(picture_names{scene_type},1));

%T1 face (noise for n=1)
jpeg1 = imread([picDir_SF 'SF' num2str(randi(10)) '.jpg']);
%to resize jpeg1%%%%
jpeg1 = imresize(jpeg1,[x_size,y_size]);

%%%%%%%%%%%%%%%%%%%%
% pad1=ones((256-x)/2,y)*double(jpeg1(1,1));
% pad2=ones(256,(256-y)/2)*double(jpeg1(1,1));
% jpeg1=[pad1;jpeg1;pad1];
% jpeg1=[pad2 jpeg1 pad2];
face_type=0; face_r=0;
if Flip==2  %flip face
    jpeg1=flipud(jpeg1);
end;


%T1 scene (noise for n=1)

jpeg2 = imread([picDir_SR 'SS' num2str(randi(10)) '.jpg']); %start with random scene
jpeg2=jpeg2(1:256,1:256); %jpeg_ends=jpeg2;
%to resize jpeg2%%%%%%%%%
jpeg2=imresize(jpeg2,[x_size,y_size]);
% pad1=ones((256-x)/2,y)*double(jpeg2(1,1));
% pad2=ones(256,(256-y)/2)*double(jpeg2(1,1));
% jpeg2=[pad1;jpeg2;pad1];
% jpeg2=[pad2 jpeg2 pad2];
%%%%%%%%%%%%%%%%%%%%%%%%%%

scene_type=0; scene_r=0;
if Flip==1  %flip scene
    jpeg2=flipud(jpeg2);
end;

%average the face and scene- at first, just scramble-
jpeg_M1=double(jpeg1)*facey+double(jpeg2)*housey;
jpeg_M1(find(jpeg_face == jpeg_face(1,1)))=255;
jpeg_ends=jpeg_M1;

if norm
    M1_mean=mean(jpeg_M1(find(jpeg_M1~=255)));
    jpeg_M1=jpeg_M1*(luminance/M1_mean);
    jpeg_M1(find(jpeg_face == jpeg_face(1,1)))=255;
    jpeg_M1=jpeg_M1+(luminance-mean(jpeg_M1(find(jpeg_M1~=255))));
end;

Screen('TextFont', window, 'Geneva');
Screen('TextSize', window, 20);
Screen('TextStyle', window, 0);

for n=1:numberoftrials
    

    
    CenterText2(window,['actual rate: ' num2str(Rate)],[0 0 0],0, 100);
    CenterText2(window,['actual duration: ' num2str(duration)],[0 0 0],0, 200);
    
    CenterText2(window,['Loading: ' num2str(round((n/numberoftrials*100))) '% completed'],[0 0 0],0, 0);
    
    Screen('Flip',window);
    
    list.jpeg{n,1}=jpeg_M1;
    % pick a random face and scene- T2 and don't let it be the same one as last
    % trial
    face_type_r2=randi(Prob); if face_type_r2>2  face_type_r2=2; end;
    face_r2=randi(size(picture_names{face_type_r2+2},1));
    while face_r==face_r2 && face_type==face_type_r2
        face_r2=randi(size(picture_names{face_type_r2+2},1));
    end;
    % and again for scenes
    scene_type_r2=randi(Prob); if scene_type_r2>2  scene_type_r2=2; end;
    scene_r2=randi(size(picture_names{scene_type_r2},1));
    while scene_r == scene_r2 && scene_type==scene_type_r2
        scene_r2=randi(size(picture_names{scene_type_r2},1));
    end;
    
    jpeg3 = imread([picDir_list{face_type_r2+2} picture_names{1,face_type_r2+2}(face_r2,1).name]);
    %to resize jpeg3%%%%
    jpeg3=imresize(jpeg3,[x_size,y_size]);


    if Flip == 2 %flip face
        jpeg3=flipud(jpeg3);
    end;
    
    jpeg4 = imread([picDir_list{scene_type_r2} picture_names{1,scene_type_r2}(scene_r2,1).name]);
%     if n==numberoftrials
%         jpeg4=jpeg_ends;
%     end;
    jpeg4=jpeg4(1:256,1:256);
    %to resize jpeg4%%%%%%%%%

    jpeg4=imresize(jpeg4,[x_size,y_size]);

%     pad1=ones((256-x)/2,y)*double(jpeg4(1,1));
%     pad2=ones(256,(256-y)/2)*double(jpeg4(1,1));
%     jpeg4=[pad1;jpeg4;pad1];
%     jpeg4=[pad2 jpeg4 pad2];
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    if Flip==1 %flip scene
        jpeg4=flipud(jpeg4);
    end;
    if norm
        jpeg4=double(jpeg4);
        jpeg4(find(jpeg_face==jpeg_face(1,1)))=255;
        jpeg4_mean=mean(jpeg4(find(jpeg4~=255)));
        jpeg4=jpeg4*(luminance/jpeg4_mean);
        jpeg4(find(jpeg_face==jpeg_face(1,1)))=255;
        jpeg4=jpeg4+(luminance-mean(jpeg4(find(jpeg4~=255))));
    end;
    
    
    data(n,1)=scene_type; data(n,2)=scene_r;  data(n,3)=scene_type_r2; data(n,4)=scene_r2;
    data(n,5)=face_type; data(n,6)=face_r; data(n,7)=face_type_r2; data(n,8)=face_r2;
    
    % average face and scene
    jpeg_M2=double(jpeg3)*facey+double(jpeg4)*housey;
    jpeg_M2(find(jpeg_face==jpeg_face(1,1)))=255;
    if n==numberoftrials
        jpeg_M2=jpeg_ends; data(n,3:4)=0; data(n,7:8)=0;
    end;
%     if norm
%         M2_mean=mean(jpeg_M2(find(jpeg_M2~=255)));
%         jpeg_M2=jpeg_M2*(luminance/M2_mean);
%         jpeg_M2(find(jpeg_face==jpeg_face(1,1)))=255;
%         jpeg_M2=jpeg_M2+(luminance-mean(jpeg_M2(find(jpeg_M2~=255))));
%     end;
    
    diff=double(jpeg_M2)-double(jpeg_M1); 
    
%     for j=1:25
%         jpeg=(jpeg_M1+(diff/25)*(j-1));
%         jpeg(find(jpeg_face==jpeg_face(1,1)))=255;
%         list.jpeg{n,j}=jpeg;
%     end;

    list.jpeg{n,1}=jpeg_M1;
    list.jpeg{n,2}=jpeg_M2;
    list.jpeg{n,3}=diff;
    list.jpeg{n,4}=mean(mean(abs(diff)));

    %make the T2 the T1
    jpeg_M1=jpeg_M2;
    face_type=face_type_r2;
    face_r=face_r2;
    scene_r=scene_r2;
    scene_type=scene_type_r2;
    
end;

list.pics=data(:,1:8);

%diff=double(jpeg_M2)-double(jpeg_M1);  %diff between trial n and n+1

data(numberoftrials,10)=0;
  
ttt=zeros(numberoftrials,framesPerPic); 

res_trial=0;

keyIsDown=0;

StimulusOnsetTime=GetSecs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%Get Ready Screens

% Screen('TextFont',window, 'Geneva');
% Screen('TextSize',window, 40);
% Screen('TextStyle', window, 0);
% 
% CenterText2(window,['actual rate: ' num2str(Rate)],[0 0 0],0, 100);
% CenterText2(window,['actual duration: ' num2str(duration)],[0 0 0],0, 0);

% Screen('Flip',window);

KbWait;

% Screen('Flip',window);
% 
% WaitSecs(.5);

%Screen('TextSize',window, 40);

FlushEvents('KeyDown'); 

if fMRI==1
        CenterText2(window,'Get Ready',[0 0 0 ],0,300);
        KeyPressed=''; 
        
elseif fMRI==0
       CenterText2(window,'Press any key to begin block',[0 0 0],0, 300);
end;


Screen(window,'PutImage',list.jpeg{1,1});
    
Screen('Flip',window);
    
% if fMRI==0
%     KbWait(KB_id);
 %elseif fMRI==1
     KbWait;
 %end;

Screen(window,'PutImage',list.jpeg{1,1}); 

Screen('Flip',window); 

starttime=GetSecs;

%WaitSecs(3-Rate*25);%-.100);

if fMRI
    WaitSecs(8);
end;


%%%%%%%%%%%%%%%%BEGIN TRIAL LOOP

% if fMRI
%     FORPQueueClear(psychtoolbox_trigger_id);
%     %FORPQueueClear(psychtoolbox_forp_id);
% end;


for n=1:numberoftrials
    
    res_count=0; last_resp_frame=0;
    
    for j=1:framesPerPic
        res_trial=0;
        jpeg=(list.jpeg{n,1}+(list.jpeg{n,3}/framesPerPic)*(j-1));
        jpeg(find(jpeg_face==jpeg_face(1,1)))=255;
        Screen(window,'PutImage',jpeg);

        [VBLTimestamp, StimulusOnsetTime]=Screen('Flip',window,StimulusOnsetTime+WaitTime-.010);
        
        ttt(n,j)=StimulusOnsetTime;
        
        if j==1 
            data(n,9)=StimulusOnsetTime; %WaitSecs(.800-Rate); 
        end;
        
        
        if j==framesPerPic
            WaitTime=HoldTime; 
        else
            WaitTime=frameDur; 
        end;
        
        while GetSecs-StimulusOnsetTime<(WaitTime-.030)
            %get input (if exists) and handle it
            %FlushEvents('KeyDown');
            [keyIsDown, secs, keyCode] = KbCheck;
            if keyIsDown==1 && res_trial==0 && (j-last_resp_frame>5 || last_resp_frame==0);
                data(n,10+res_count*3)=sum(find(keyCode));
                data(n,11+res_count*3)=secs;
                data(n,12+res_count*3)=j;
                last_resp_frame=j;
                keyIsDown=0;
                FlushEvents('KeyDown');
                res_trial=1;
                res_count=res_count+1;
            end
        end
        
        if data(n,10)==55 %kill is '7'
            if fMRI
                [response]=get_RTs5(data,numberoftrials,r1_MRI,r2_MRI,framesPerPic,Task);
            else
                [response]=get_RTs5(data,numberoftrials,r1,r2,framesPerPic,Task);
            end;
            endtime=GetSecs;
            C=clock;
            mkdir data/incomplete_exp/;
            filename = ['data/incomplete_exp/Data_' Prac '_' subName '_' date '_' ...
                        num2str(C(4)) '_' num2str(C(5)) '_city_mnt_v1B_beh_fMRI.mat'];
            save(filename,  'data', ... variables to be saved
                            'response', ...
                            'ttt', ...
                            'FHR', ...
                            'Rate', ...
                            'Prob', ...
                            'Task', ...
                            'Scram', ...
                            'framesPerPic', ...
                            'numberoftrials', ...
                            'subName', ...
                            'starttime', ...
                            'endtime', ...
                            'x_size', ...
                            'y_size');
            
            clear mex;
            stopitnow;
            ShowCursor;
        end;
        
        if data(n,10)==19 %pause is 'p'
            FlushEvents;
            WaitSecs(.5);
            KbWait;
            FlushEvents;
        end;
    end;
end;

if fMRI
    WaitSecs(8);
end;

Screen('Flip',window);
endtime=GetSecs;
ShowCursor;

%%%%%%%%%%%%%%%% analyze and save data %%%%%%%%%%%%%%%%%%

if fMRI
    [response]=get_RTs5(data,numberoftrials,r1_MRI,r2_MRI,framesPerPic,Task);
else
    [response]=get_RTs5(data,numberoftrials,r1,r2,framesPerPic,Task);
end;

clear mex;
C=clock;
mkdir data;
'saving data- wait!'
filename = ['data/Data_' Prac '_' subName '_' date '_' num2str(C(4)) '_' num2str(C(5)) '_city_mnt_v1B_beh_fMRI.mat'];
save(filename,  'data', ... variables to be saved
                'response', ...
                'ttt', ...
                'FHR', ...
                'Rate', ...
                'Prob', ...
                'Task', ...
                'Scram', ...
                'framesPerPic', ...
                'numberoftrials', ...
                'subName', ...
                'starttime', ...
                'endtime', ...
                'x_size', ...
                'y_size');
WaitSecs(1);

%Kbwait;
Screen('CloseAll');